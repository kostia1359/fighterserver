const { FightRepository } = require('../repositories/fightRepository');

class FightersService {
    postFight(logs) {
        const data = { logs };

        return FightRepository.create(data);
    }

    getAll() {
        return FightRepository.getAll();
    }

    deleteFight(id) {
        const fight = FightRepository.delete(id);

        if (fight.length === 0) {
            return null;
        }

        return fight;
    }

    search(search) {
        const item = FightRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

}

module.exports = new FightersService();