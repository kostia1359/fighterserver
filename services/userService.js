const { UserRepository } = require('../repositories/userRepository');

class UserService {
    getAll() {
        return UserRepository.getAll();
    }

    updateUser(id, data) {
        if (!this.search({ id })) {
            return null;
        }

        this.uniquenessEmailCheck(data.email);

        return UserRepository.update(id, data);
    }

    postUser(data) {
        this.uniquenessEmailCheck(data.email);

        return UserRepository.create(data);
    }

    deleteUser(id) {
        const user = UserRepository.delete(id);

        if (user.length === 0) {
            return null;
        }

        return user;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    uniquenessEmailCheck(email){
        const user=this.search({ email })

        if(user){
            throw Error('email should be unique to identify user');
        }
    }
}

module.exports = new UserService();