const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    getAll() {
        return FighterRepository.getAll();
    }

    updateFighter(id, data) {
        if (!this.search({ id })) {
            return null;
        }

        return FighterRepository.update(id, data);
    }

    postFighter(data) {
        return FighterRepository.create(data);
    }

    deleteFighter(id) {
        const fighter = FighterRepository.delete(id);

        if (fighter.length === 0) {
            return null;
        }

        return fighter;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();