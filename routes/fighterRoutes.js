const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');
const { checkError404 }=require('../helpers/CheckError404')

const router = Router();

router.get('/', function (request, response, next) {
    response.data = FighterService.getAll();

    next();
})

router.get('/:id', function (request, response, next) {
    const id = request.params.id;
    const fighter = FighterService.search({id});

    if (!checkFighterError404(response, fighter)) {
        response.data = fighter;
    }

    next();
})

router.post('/', createFighterValid, function (request, response, next) {
    FighterService.postFighter(response.data);

    next()
})

router.put('/:id', updateFighterValid, function (request, response, next) {
    const updatedFighter = FighterService.updateFighter(request.params.id, response.data);

    if (!checkFighterError404(response, updatedFighter)) {
        response.data = updatedFighter;
    }

    next();
});

router.delete('/:id', function (request, response, next) {
    const id = request.params.id;
    const deletedFighter = FighterService.deleteFighter(id);

    if (!checkFighterError404(response, deletedFighter)) {
        response.data = deletedFighter;
    }

    next();
})

router.use(responseMiddleware);

module.exports = router;

function checkFighterError404(response, fighter) {
    return checkError404(response, fighter, 'Fighter');
}