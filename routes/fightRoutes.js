const { Router } = require('express');
const FightService = require('../services/fightService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { checkError404 }=require('../helpers/CheckError404')


const router = Router();

router.get('/', function (request, response, next) {
    response.data = FightService.getAll();

    next();
})

router.get('/:id', function (request, response, next) {
    const id = request.params.id;
    const fight = FightService.search({id});

    if (!checkFightError404(response, fight)) {
        response.data = fight;
    }

    next();
})

router.post('/', function (request, response, next) {
    FightService.postFight(request.body);

    next()
})

router.delete('/:id', function (request, response, next) {
    const id = request.params.id;

    const deletedFight = FightService.deleteFight(id);

    if (!checkFightError404(response, deletedFight)) {
        response.data = deletedFight;
    }

    next();
})

router.use(responseMiddleware);

module.exports = router;

function checkFightError404(response, fighter) {
    return checkError404(response, fighter, 'Fight');
}