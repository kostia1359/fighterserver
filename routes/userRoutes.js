const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { checkError404 }=require('../helpers/CheckError404')

const router = Router();

router.get('/', function (request, response, next) {
    response.data = UserService.getAll();

    next();
})

router.get('/:id', function (request, response, next) {
    const id = request.params.id;
    const user = UserService.search({id});

    if (!checkUserError404(response, user)) {
        response.data = user;
    }

    next();
})

router.post('/', createUserValid, function (request, response, next) {
    try {
        UserService.postUser(response.data);
    }catch (e) {
        response.status(400);
        response.err=e;
    }

    next()
})

router.put('/:id', updateUserValid, function (request, response, next) {
    try {
        const updatedUser = UserService.updateUser(request.params.id, response.data);

        if (!checkUserError404(response, updatedUser)) {
            response.data = updatedUser;
        }
    }
    catch (e) {
        response.status(400);
        response.err=e;
    }

    next();
});

router.delete('/:id', function (request, response, next) {
    const id = request.params.id;
    const deletedUser = UserService.deleteUser(id);

    if (!checkUserError404(response, deletedUser)) {
        response.data = deletedUser;
    }

    next();
})

router.use(responseMiddleware);

module.exports = router;


function checkUserError404(response, user) {
    return checkError404(response, user, 'User');
}
