function checkError404(response, objToCheck, modelName) {
    if (!objToCheck) {
        response.status(404);
        response.err = Error(`${modelName} not found`);
        return true;
    }
    return false;
}

exports.checkError404 = checkError404;