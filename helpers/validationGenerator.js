function checkIdField(obj, modelName) {
    if (obj.hasOwnProperty('id')) {
        throw Error(`${modelName} should not contain Id field`);
    }
}

function checkFieldsUpdate(obj, model, modelName) {
    checkIdField(obj, modelName);

    const givenFields = {...model}
    delete givenFields.id;

    for (let key in obj) {
        if (!givenFields.hasOwnProperty(key)) {
            throw Error(`${modelName} has extra field`);
        }
    }
}

function checkFieldsCreate(obj, model, modelName) {
    checkFieldsUpdate(obj, model, modelName);

    if (Object.keys(obj).length !== Object.keys(model).length - 1) {
        throw Error(`${modelName} does not contain all necessary fields`);
    }
}


function validate(obj, validator) {
    for (let key in obj) {
        if (validator.hasOwnProperty(key)) {
            validator[key](obj[key]);
        }
    }
}


function setResponseData(res, objToValidate, checkFields, model, modelName, validator) {
    try {
        checkFields(objToValidate, model, modelName);
        validate(objToValidate, validator);

        res.data = objToValidate;
    } catch (e) {
        res.status(400);
        res.err = e;

        return false;
    }

    return true;
}

function setResponseDataUpdate(res, obj, model, modelName, validator) {
    return setResponseData(res, obj, checkFieldsUpdate, model, modelName, validator);
}

function setResponseDataCreate(res, obj, model, modelName, validator) {
    return setResponseData(res, obj, checkFieldsCreate, model, modelName, validator);
}

exports.setResponseDataCreate = setResponseDataCreate;
exports.setResponseDataUpdate = setResponseDataUpdate;
