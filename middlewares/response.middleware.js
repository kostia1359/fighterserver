const responseMiddleware = (req, res, next) => {
    res.set('Content-Type', 'application/json');

    if (res.err) {
        res.send({
            error: true,
            message: res.err.message
        });
    } else {
        res.status(200);
        res.send(res.data);
    }

    next();
}

exports.responseMiddleware = responseMiddleware;