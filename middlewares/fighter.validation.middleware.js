const { fighter } = require('../models/fighter');
const { setResponseDataCreate, setResponseDataUpdate } = require('../helpers/validationGenerator');

const validator = {
    power: function (str) {
        isFieldNumeric(str, 'power');
        isNumberInRange(str, 1, 99, 'power');
    },
    health: function (str) {
        isFieldNumeric(str, 'health');
        isNumberInRange(str, 1, 100, 'power');
    },
    defense: function (str) {
        isFieldNumeric(str, 'defense');
        isNumberInRange(str, 1, 10, 'power');
    },
    name: function (str) {
        if (str.length === 0) {
            throw Error('name should not be empty');
        }
    },

}

const createFighterValid = (req, res, next) => {
    const fighterToValidate=req.body;

    if(!fighterToValidate.health){
        fighterToValidate.health='100';
    }

    validateFighter(res, setResponseDataCreate, fighterToValidate, next);
}

const updateFighterValid = (req, res, next) => {
    const fighterToValidate=req.body;

    validateFighter(res, setResponseDataUpdate, fighterToValidate, next);
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;

function isFieldNumeric(str, fieldName) {
    const isNumeric = /^[0-9]+$/.test(str);

    if (!isNumeric) {
        throw Error(`${fieldName} should be a number`);
    }
}

function isNumberInRange(str, min, max, fieldName) {
    const number = Number(str);

    if (number < min || number > max) {
        throw Error(`${fieldName} should be in range [${min}, ${max}]`);
    }
}

function validateFighter(res, setDataFunction, fighterToValidate, next) {
    const isFighterValid = setDataFunction(res, fighterToValidate, fighter, 'Fighter', validator);

    if(!isFighterValid){
        next('route');
    }else{
        next();
    }
}
