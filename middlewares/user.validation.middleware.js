const { user } = require('../models/user');
const { setResponseDataCreate, setResponseDataUpdate } = require('../helpers/validationGenerator');

const validator={
    email:function (str) {
        const isGmail=/^[a-zA-Z0-9/.]+@gmail.com$/.test(str);
        if(!isGmail) {
            throw Error('email should in gmail.com domain');
        }
    },
    phoneNumber:function (str) {
        const isPhoneValid=/^\+380[0-9]{9}$/.test(str);
        if(!isPhoneValid) {
            throw Error('phone number format is invalid');
        }
    },
    password:function (str) {
        if(str.length<3) {
            throw Error('password length should be greater than 3');
        }
    },
    firstName:function (str) {
        if(str.length===0) {
            throw Error('first Name should not be empty');
        }
    },
    lastName:function (str) {
        if(str.length===0) {
            throw Error('first Name should not be empty');
        }
    }
}

const createUserValid = (req, res, next) => {
    const userToValidate=req.body;

    validateUser(res, setResponseDataCreate, userToValidate, next);
}

const updateUserValid = (req, res, next) => {
    const userToValidate=req.body;

    validateUser(res, setResponseDataUpdate, userToValidate, next);
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;

function validateUser(res, setDataFunction, userToValidate, next) {
    const isUserValid = setDataFunction(res, userToValidate, user, 'User', validator);

    if(!isUserValid){
        next('route');
    }else{
        next();
    }
}